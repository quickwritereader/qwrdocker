## Calibre and I.L.A

####  PLEASE NOTE THAT 
	- this version is only intended to demonstrate voice control      
           capabilities of system and has limitations                     
	- you can add any book manually using calibre                     
	- commands only work for books which you added from calibre       
	- "hey ILA" command required before every command                  
	- this version uses default acoustic model and voice activity     
 
#### For  optimal sized docker image check the following repository 

See [ qwrdocker_optim ]( https://bitbucket.org/quickwritereader/qwrdocker_optim )

#### For installing directly on Debian (tested only with Ubuntu) run installOnDebian.sh
See [installOnDebian.sh]( https://bitbucket.org/quickwritereader/qwrdocker/raw/b0ad739c07216d8ac6e316557a1a670bdb322e57/installOnDebian.sh )

#### Building using docker 
     ./build.sh


#### RUN usin docker ###
     ./runDocker.sh

#### Usage guide ###
 
	- Before every command activate speech recognizer by saying "hey Ila"
	- To list titles say "list titles"
	- To list authors say "list authors"
	- To list genres say "list genre"
	- Open books with commands "open 'book name'" 
		  for example: "open the man outside"
	- To list books by authors say "books by <author name>"
		  for example: "books by Franz Kafka"
	- To list books by genre say "genre <genre name>"
		  for example: "genre technical"
	- To read a book say "read" or "read page"
	- To navigate pages say "previous" or "previous page" , "next" or "next page"
	- To stop tts say "stop" or "stop speaking"
	- To resume tts say "resume" or "resume speaking"