#!/bin/bash
 

echo "we try to install I.L.A and Calibre demo version"
echo "we assume /usr/local/bin in you PATH environment variable"

#environment variables that could be used
export JAVA_HOME=/usr/lib/jvm/java-8-oracle
export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH
export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig:$LD_LIBRARY_PATH


homeDir=$(echo ~)

#install packages
echo "please wait ..."
sudo   apt-get update -y -qq  && \
	sudo apt-get install  -y -qq git python xdg-utils imagemagick  libtool  swig  python-dev  bison  gcc  automake autoconf pkg-config \
	libpulse-dev make xz-utils alsa-utils   usbutils

JAVA_VER=$(java -version 2>&1 | sed 's/java version "\(.*\)\.\(.*\)\..*"/\1\2/; 1q')


#install java
if [ "$JAVA_VER" -ge 15 ]; then
	echo "java  $JAVA_VER is already installed"
else
	echo "we try to install java"
	sudo apt-get install -y -qq software-properties-common && \
	sudo echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo debconf-set-selections && \
	sudo add-apt-repository -y ppa:webupd8team/java && \
	sudo apt-get update -qq -y && \
	sudo apt-get install -y  oracle-java8-installer 
fi

#use temporary directory
cd /tmp/

if [ ! -d "/tmp/qwrdocker" ]; then
	echo "get files with git"
	#get files
	git clone https://quickwritereader@bitbucket.org/quickwritereader/qwrdocker.git
else
   echo "file exists"	
fi

#make directory
mkdir -p  "$homeDir/Calibre Library/"   
mkdir -p  "$homeDir/ila/"   

#tar calibre into locla/bin
sudo tar xJf /tmp/qwrdocker/calibre.tar.xz -C /usr/local/bin  
tar xJf /tmp/qwrdocker/ila.tar.xz -C  ~/ila
tar xJf /tmp/qwrdocker/clibrary.tar.xz -C "$homeDir/Calibre Library/" --strip-components=1 
tar xJf /tmp/qwrdocker/pocketSphinxSource.tar.xz -C  /tmp/pocketSphinxSource/ --strip-components=1 


cd /tmp/pocketSphinxSource/sphinxbase  &&  sh autogen.sh && make && sudo  make install  
cd /tmp/pocketSphinxSource/pocketsphinx  && sh autogen.sh && make && sudo  make install 

#make ila/start.sh runnable 
sudo chmod +x "$homeDir/ila/start.sh"  

#create in local/bin/start
cat <<EOF > "/tmp/ila_calibre.sh" 
#!/bin/bash  
cd "$homeDir/ila"  
./start.sh  
EOF

sudo mv /tmp/ila_calibre.sh /usr/local/bin/ila_calibre.sh
sudo chmod +x "/usr/local/bin/ila_calibre.sh"
echo "To run just execute in terminal ila_calibre.sh" 
        
 
