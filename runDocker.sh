#!/bin/bash  
xhost +local:  
 docker run -it  -v /tmp/.X11-unix:/tmp/.X11-unix  \
    -e DISPLAY=unix$DISPLAY  \
     --device /dev/snd --device /dev/bus/usb  \
    calibre_ila $@
xhost -local:
