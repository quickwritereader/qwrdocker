
FROM ubuntu:14.04
env DEBIAN_FRONTEND=noninteractive


#add user
RUN groupadd -r calibre && useradd -g calibre -d /home/calibre -r -m calibre &&\
gpasswd -a calibre audio &&\
gpasswd -a calibre audio &&\
echo "root:calibre" | chpasswd &&\
echo "calibre:calibre" | chpasswd
 

ENV HOME /home/calibre
WORKDIR /tmp




#install java
RUN \
  apt-get install -y software-properties-common && \
  echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections && \
  add-apt-repository -y ppa:webupd8team/java && \
  apt-get update && \
  apt-get install -y oracle-java8-installer && \
  rm -rf /var/lib/apt/lists/* && \
  rm -rf /var/cache/oracle-jdk8-installer && apt-get clean 

 
 

#copy files
COPY *.tar.xz ./  


RUN  mkdir -p  /home/calibre/ &&\
     mkdir -p "/home/calibre/Calibre Library/" &&\
     mkdir -p "/home/calibre/ila/" &&\
     mkdir -p "/home/calibre/pocketSphinxSource/" &&\
     tar xJf calibre.tar.xz -C /home/calibre  &&\
     rm calibre.tar.xz  &&\
     tar xJf ila.tar.xz -C  /home/calibre/ila    &&\
     rm ila.tar.xz  &&\
     tar xJf clibrary.tar.xz -C "/home/calibre/Calibre Library/" --strip-components=1 &&\
     rm clibrary.tar.xz &&\
     chown -R calibre:calibre "/home/calibre" &&\
     chown  -R  calibre:calibre "/home/calibre/Calibre Library/"   &&\
     mkdir -p /home/calibre/data && chown  -R  calibre:calibre /home/calibre/data 

#install pocketsphinx and audio then remove not needed

ENV JAVA_HOME /usr/lib/jvm/java-8-oracle
ENV LD_LIBRARY_PATH /usr/local/lib
ENV PKG_CONFIG_PATH /usr/local/lib/pkgconfig

RUN  tar xJf pocketSphinxSource.tar.xz -C  /home/calibre/pocketSphinxSource --strip-components=1  &&\
    rm pocketSphinxSource.tar.xz &&\
    apt-get update -y -qq && \
    apt-get install -y -qq wget python xdg-utils imagemagick  libtool  swig  python-dev  bison  gcc  automake autoconf pkg-config  libpulse-dev build-essential xz-utils alsa-utils   usbutils  && apt-get clean &&\
    cd /home/calibre/pocketSphinxSource/sphinxbase && ls && ./autogen.sh && make && make install &&\
    cd /home/calibre/pocketSphinxSource/pocketsphinx &&  ./autogen.sh && make && make install &&\
    apt-get remove -y -qq xz-utils  libtool  swig  python-dev  bison  gcc  automake autoconf pkg-config  libpulse-dev &&\
    apt-get clean && apt-get clean autoclean &&\
    apt-get autoremove -y &&\
    rm -rf /var/lib/*  &&\
    rm -rf /home/calibre/pocketSphinxSource
 
 
 

ENV PATH $PATH:/home/calibre/:/home/calibre/ila
WORKDIR /home/calibre/ila  
USER calibre
CMD   ./start.sh 

